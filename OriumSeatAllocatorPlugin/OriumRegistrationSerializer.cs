﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anima.Orium.Plugins
{
    class OriumRegistrationSerializer : OriumBaseSerializer
    {
        public override string MessageId
        {
            get { return "Orium.Registration"; }
        }

        public override void Serialize(object packet, System.IO.BinaryWriter bw)
        {
            bw.Write((bool)packet);
        }

        public override object Deserialize(System.IO.BinaryReader br)
        {
            return br.ReadBoolean();
        }
    }
}
