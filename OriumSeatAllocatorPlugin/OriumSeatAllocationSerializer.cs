﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anima.Orium;

namespace Anima.Orium.Plugins
{
    class OriumSeatAllocationSerializer : OriumBaseSerializer
    {
        public override string MessageId
        {
            get { return "Orium.SeatAllocation"; }
        }

        public override void Serialize(object packet, System.IO.BinaryWriter bw)
        {
            bw.Write((int)packet);
        }

        public override object Deserialize(System.IO.BinaryReader br)
        {
            return br.ReadInt32();
        }
    }
}
