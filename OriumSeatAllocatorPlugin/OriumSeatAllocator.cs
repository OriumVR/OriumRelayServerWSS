﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anima.Orium.Plugins
{
    public class OriumSeatAllocator : IOriumServerPlugin
    {
        private static List<OriumProtocol> Protocols = new List<OriumProtocol>();

        private static Dictionary<int, OriumProtocol> SeatedClients = new Dictionary<int, OriumProtocol>();

        private int GetFirstAvailableSeat()
        {
            int seatIndex = 0;
            while (SeatedClients.ContainsKey(seatIndex))
            {
                seatIndex++;
            }
            return seatIndex;
        }

        private int FindClientSeat(OriumProtocol protocol)
        {
            foreach(var seatedClient in SeatedClients)
            {
                if(seatedClient.Value == protocol)
                {
                    return seatedClient.Key;
                }
            }
            return -1;
        }

        private void OnRegistrationReceived(object packet, OriumProtocol protocol)
        {
            bool isAudience = (bool)packet;

            if(isAudience)
            {
                int firstAvailableSeat = GetFirstAvailableSeat();
                SeatedClients.Add(firstAvailableSeat, protocol);

                protocol.Send(typeof(OriumSeatAllocationSerializer), firstAvailableSeat);

                Console.WriteLine("Audience Member Allocated to Seat " + firstAvailableSeat);

                // Inform all other protocols that the seat is now occupied
                foreach(OriumProtocol protocolToInform in Protocols)
                {
                    if(protocolToInform != protocol)
                    {
                        protocolToInform.Send(typeof(OriumSeatOccupationSerializer), new OriumSeatOccupationPacket
                        {
                            SeatIndex = firstAvailableSeat,
                            IsOccupied = true
                        });
                    }
                }
            }

            // Inform the new protocol of all the other seats that were already occupied
            foreach (var seatedClient in SeatedClients)
            {
                if (seatedClient.Value != protocol)
                {
                    protocol.Send(typeof(OriumSeatOccupationSerializer), new OriumSeatOccupationPacket
                    {
                        SeatIndex = seatedClient.Key,
                        IsOccupied = true
                    });
                }
            }
        }

        public void OnClientConnected(OriumProtocol protocol)
        {
            protocol.RegisterCallback(typeof(OriumRegistrationSerializer), (long timestamp, ulong channel, object packet) =>
            {
                OnRegistrationReceived(packet, protocol);
            });

            Protocols.Add(protocol);
        }

        public void OnClientDisconnected(OriumProtocol protocol)
        {
            int clientSeat = FindClientSeat(protocol);
            if(clientSeat > -1)
            {
                SeatedClients.Remove(clientSeat);

                Console.WriteLine("Audience Member Removed from Seat " + clientSeat);

                // Inform all other protocols that the seat is no longer occupied
                foreach (OriumProtocol protocolToInform in Protocols)
                {
                    if (protocolToInform != protocol)
                    {
                        protocolToInform.Send(typeof(OriumSeatOccupationSerializer), new OriumSeatOccupationPacket
                        {
                            SeatIndex = clientSeat,
                            IsOccupied = true
                        });
                    }
                }
            }

            Protocols.Remove(protocol);
        }

    }
}
