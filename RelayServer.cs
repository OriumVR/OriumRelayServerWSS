using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using System.Text;
using System.Diagnostics;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace Anima.Orium
{
    static class PluginBank
    {
        public static readonly List<IOriumServerPlugin> Plugins;

        static PluginBank()
        {
            Plugins = new List<IOriumServerPlugin>(GenericPluginLoader<IOriumServerPlugin>.LoadPlugins("Plugins"));
        }
    }

    class RelayBehaviour : WebSocketBehavior
    {
        public static OriumRegistry Registry = new OriumRegistry();

//#error Plugins should not be using their own lists of protocols, and the main list should be made THREAD SAFE.
        private static List<OriumProtocol> Protocols = new List<OriumProtocol>();

        private OriumProtocol _protocol;

        protected override void OnOpen()
        {
            base.OnOpen();

            _protocol = new OriumProtocol(Registry, new OriumWSSTransportLayer(Context.WebSocket));
            _protocol.RegisterCallback(typeof(OriumPacketDropSerializer), (long timestamp, ulong channel, object packet) =>
            {
                PacketDropPacket dropPacket = (PacketDropPacket)packet;
                Console.WriteLine("Packet Drop Ratio: " + dropPacket.Ratio);
            });
            _protocol.RegisterFallback((long timestamp, ulong hash, ulong channel, byte[] data) =>
            {
                foreach (OriumProtocol protocol in Protocols)
                {
                    if (protocol != _protocol)
                    {
                        protocol.Send(hash, data, channel, timestamp);
                    }
                }
            });

            foreach (IOriumServerPlugin plugin in PluginBank.Plugins)
            {
                plugin.OnClientConnected(_protocol);
            }
            
            Protocols.Add(_protocol);
        }

        protected override void OnClose(CloseEventArgs e)
        {
            base.OnClose(e);

            foreach (IOriumServerPlugin plugin in PluginBank.Plugins)
            {
                plugin.OnClientDisconnected(_protocol);
            }

            Protocols.Remove(_protocol);
        }
    }

    class RelayServer
    {
        public static void Main()
        {
            foreach (IOriumServerPlugin plugin in PluginBank.Plugins)
            {
                Console.WriteLine("Plugin Loaded: " + plugin.GetType().Name);
            }

            WebSocketServer server = new WebSocketServer(2525);
            server.AddWebSocketService<RelayBehaviour>("/");
            server.Start();

            // Keep this process running until Enter is pressed
            Console.WriteLine("Press Enter to quit...");
            Console.ReadLine();

            server.Stop();
        }
    }
}
