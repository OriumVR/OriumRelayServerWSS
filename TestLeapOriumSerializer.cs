﻿using System.Collections;
using System.IO;
using Anima.Orium;

public struct TestLeapOriumPacket
{
    public bool LeftHand;
    public float[] Matrix;
    public float FingerDistance;
}

public class TestLeapOriumSerializer : IOriumSerializer {

    public string MessageId {
        get { return "TestLeapMsg"; }
    }

    public void Serialize(object packet, BinaryWriter bw)
    {
        TestLeapOriumPacket leapPacket = (TestLeapOriumPacket)packet;

        if (leapPacket.LeftHand)
        {
            bw.Write('L');
        }
        else
        {
            bw.Write('R');
        }

        for (int i = 0; i < 16; ++i)
        {
            bw.Write(leapPacket.Matrix[i]);
        }

        bw.Write(leapPacket.FingerDistance);
    }

    public object Deserialize(BinaryReader br)
    {
        bool leftHand = br.ReadChar () == 'L';

        float[] matrix = new float[16];
        for (int i = 0; i < 16; ++i) {
            matrix [i] = br.ReadSingle ();
        }

        float fingerDistance = br.ReadSingle ();

        return new TestLeapOriumPacket
        {
            LeftHand = leftHand,
            Matrix = matrix,
            FingerDistance = fingerDistance
        };
    }
}
