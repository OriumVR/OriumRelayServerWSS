namespace VoiceChat
{
    public enum VoiceChatCompression : byte
    {
        /*
        Raw, 
        RawZlib, 
        */
        Alaw,
        AlawZlib,
        Speex
    } 
}
