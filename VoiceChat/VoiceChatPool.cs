using System.Collections.Generic;

namespace VoiceChat
{
    public abstract class VoiceChatPool<T>
    {
        Queue<T[]> queue = new Queue<T[]>();
        private int currentSampleSize = 0;

        public T[] Get(int sampleSize)
        {
            if(sampleSize != currentSampleSize)
            {
                queue.Clear();
            }

            if (queue.Count > 0)
            {
                return queue.Dequeue();
            }

            return new T[sampleSize];
        }

        public void Return(T[] obj)
        {
            if (obj != null)
            {
                queue.Enqueue(obj);
            }
        }
    }

    public class VoiceChatBytePool : VoiceChatPool<byte>
    {
        public static readonly VoiceChatBytePool Instance = new VoiceChatBytePool();
    }

    public class VoiceChatShortPool : VoiceChatPool<short>
    {
        public static readonly VoiceChatShortPool Instance = new VoiceChatShortPool();
    }

    public class VoiceChatFloatPool : VoiceChatPool<float>
    {
        public static readonly VoiceChatFloatPool Instance = new VoiceChatFloatPool();
    } 
}