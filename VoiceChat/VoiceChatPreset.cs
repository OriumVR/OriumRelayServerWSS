namespace VoiceChat
{
    public enum VoiceChatPreset
    {
        Speex_8K,
        Speex_16K,

        Alaw_4k,
        Alaw_8k,
        Alaw_16k,
        Alaw_Zlib_4k,
        Alaw_Zlib_8k,
        Alaw_Zlib_16k,

        /*
        Raw_4k,
        Raw_8k,
        Raw_16k,
        Raw_Zlib_4k,
        Raw_Zlib_8k,
        Raw_Zlib_16k,
        */
    } 

    public class VoiceChatPresetParams
    {
        public static void GetParameters(VoiceChatPreset preset, out int Frequency, out int SampleSize, out VoiceChatCompression Compression)
        {
            // Default to Speex_16K
            Frequency = 16000;
            SampleSize = 640;
            Compression = VoiceChatCompression.Speex;

            switch (preset)
            {
            case VoiceChatPreset.Speex_8K:
                Frequency = 8000;
                SampleSize = 320;
                Compression = VoiceChatCompression.Speex;
                break;

            case VoiceChatPreset.Speex_16K:
                Frequency = 16000;
                SampleSize = 640;
                Compression = VoiceChatCompression.Speex;
                break;

            case VoiceChatPreset.Alaw_4k:
                Frequency = 4096;
                SampleSize = 128;
                Compression = VoiceChatCompression.Alaw;
                break;

            case VoiceChatPreset.Alaw_8k:
                Frequency = 8192;
                SampleSize = 256;
                Compression = VoiceChatCompression.Alaw;
                break;

            case VoiceChatPreset.Alaw_16k:
                Frequency = 16384;
                SampleSize = 512;
                Compression = VoiceChatCompression.Alaw;
                break;

            case VoiceChatPreset.Alaw_Zlib_4k:
                Frequency = 4096;
                SampleSize = 128;
                Compression = VoiceChatCompression.AlawZlib;
                break;

            case VoiceChatPreset.Alaw_Zlib_8k:
                Frequency = 8192;
                SampleSize = 256;
                Compression = VoiceChatCompression.AlawZlib;
                break;

            case VoiceChatPreset.Alaw_Zlib_16k:
                Frequency = 16384;
                SampleSize = 512;
                Compression = VoiceChatCompression.AlawZlib;
                break;

                /*
                    case VoiceChatPreset.Raw_4k:
                        Frequency = 4096;
                        SampleSize = 128;
                        Compression = VoiceChatCompression.Raw;
                        break;

                    case VoiceChatPreset.Raw_8k:
                        Frequency = 8192;
                        SampleSize = 256;
                        Compression = VoiceChatCompression.Raw;
                        break;

                    case VoiceChatPreset.Raw_16k:
                        Frequency = 16384;
                        SampleSize = 512;
                        Compression = VoiceChatCompression.Raw;
                        break;

                    case VoiceChatPreset.Raw_Zlib_4k:
                        Frequency = 4096;
                        SampleSize = 128;
                        Compression = VoiceChatCompression.RawZlib;
                        break;

                    case VoiceChatPreset.Raw_Zlib_8k:
                        Frequency = 8192;
                        SampleSize = 256;
                        Compression = VoiceChatCompression.RawZlib;
                        break;

                    case VoiceChatPreset.Raw_Zlib_16k:
                        Frequency = 16384;
                        SampleSize = 512;
                        Compression = VoiceChatCompression.RawZlib;
                        break;
                        */
            }
        }
    }
}
