﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoiceChat;

namespace SampleVS2012CSharp
{
    class VoiceChatRecorder
    {
        bool autoDetectSpeaking = false;
        int autoDetectIndex = 4;
        //float forceTransmitTime = 2f;

        int sampleIndex = 0;
        bool transmitToggled = false;
        bool recording = false;
        //float forceTransmit = 0f;
        int recordFrequency = 0;
        int recordSampleSize = 0;
        int targetFrequency = 0;
        int targetSampleSize = 0;
        float[] fftBuffer = null;
        float[] sampleBuffer = null;
        VoiceChatCircularBuffer<float[]> previousSampleBuffer = new VoiceChatCircularBuffer<float[]>(5);

        public bool AutoDetectSpeech
        {
            get { return autoDetectSpeaking; }
            set { autoDetectSpeaking = value; }
        }

        public event System.Action<VoiceChatPacket> NewSample;

        public bool StartRecording(VoiceChatPreset preset, int recordFrequency)
        {
            if (recording)
            {
                Console.WriteLine("Already recording");
                return false;
            }

            VoiceChatCompression compression;
            VoiceChatPresetParams.GetParameters(preset, out targetFrequency, out targetSampleSize, out compression);

            this.recordFrequency = recordFrequency;
            recordSampleSize = recordFrequency / (targetFrequency / targetSampleSize);

            sampleBuffer = new float[recordSampleSize];
            fftBuffer = new float[VoiceChatUtils.ClosestPowerOfTwo(targetSampleSize)];
            recording = true;

            return recording;
        }

        public void StopRecording()
        {
            recording = false;
        }

        void Resample(float[] src, float[] dst)
        {
            if (src.Length == dst.Length)
            {
                Array.Copy(src, 0, dst, 0, src.Length);
            }
            else
            {
                //TODO: Low-pass filter 
                float rec = 1.0f / (float)dst.Length;

                for (int i = 0; i < dst.Length; ++i)
                {
                    float interp = rec * (float)i * (float)src.Length;
                    dst[i] = src[(int)interp];
                }
            }
        }

        public void Update(float[] inputBuffer, VoiceChatPreset preset)
        {
            if (!recording)
            {
                return;
            }

            //forceTransmit -= deltaTime;

            bool transmit = transmitToggled;

            while (sampleIndex + recordSampleSize <= inputBuffer.Length)
            {
                Buffer.BlockCopy(inputBuffer, sampleIndex, sampleBuffer, 0, recordSampleSize * 4);

                ProcessSample(preset, transmit);
            }
        }

        void ProcessSample(VoiceChatPreset preset, bool transmit)
        {
            int frequency, sampleSize;
            VoiceChatCompression compression;
            VoiceChatPresetParams.GetParameters(preset, out frequency, out sampleSize, out compression);

            // Grab a new sample buffer
            float[] targetSampleBuffer = VoiceChatFloatPool.Instance.Get(sampleSize);

            // Resample our real sample into the buffer
            Resample(sampleBuffer, targetSampleBuffer);

            // Forward index
            sampleIndex += recordSampleSize;

            // Highest auto-detected frequency
            float freq = float.MinValue;
            int index = -1;

            // Auto detect speech, but no need to do if we're pushing a key to transmit
            if (autoDetectSpeaking && !transmit)
            {
                // Clear FFT buffer
                for (int i = 0; i < fftBuffer.Length; ++i)
                {
                    fftBuffer[i] = 0;
                }

                // Copy to FFT buffer
                Array.Copy(targetSampleBuffer, 0, fftBuffer, 0, targetSampleBuffer.Length);

                // Apply FFT
                Exocortex.DSP.Fourier.FFT(fftBuffer, fftBuffer.Length / 2, Exocortex.DSP.FourierDirection.Forward);

                // Get highest frequency
                for (int i = 0; i < fftBuffer.Length; ++i)
                {
                    if (fftBuffer[i] > freq)
                    {
                        freq = fftBuffer[i];
                        index = i;
                    }
                }
            }

            // If we have an event, and 
            //if (NewSample != null && (transmit || forceTransmit > 0 || index >= autoDetectIndex))
            if (NewSample != null && (transmit|| index >= autoDetectIndex))
            {
                // If we auto-detected a voice, force recording for a while
                if (index >= autoDetectIndex)
                {
                    //if (forceTransmit <= 0)
                    //{
                        //while (previousSampleBuffer.Count > 0)
                        //{
                        //    TransmitBuffer(previousSampleBuffer.Remove(), preset);
                        //}
                    //}

                    //forceTransmit = forceTransmitTime;
                }

                TransmitBuffer(targetSampleBuffer, preset);
            }
            else
            {
                if (previousSampleBuffer.Count == previousSampleBuffer.Capacity)
                {
                    VoiceChatFloatPool.Instance.Return(previousSampleBuffer.Remove());
                }

                previousSampleBuffer.Add(targetSampleBuffer);
            }

        }

        void TransmitBuffer(float[] buffer, VoiceChatPreset preset)
        {
            // Raise event
            NewSample(VoiceChatUtils.Compress(buffer, preset));
        }

    }
}
